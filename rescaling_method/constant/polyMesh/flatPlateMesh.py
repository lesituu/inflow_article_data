#! /sw/comp/python/2.7.2_tintin/bin/python
from __future__ import division
from subprocess import call
from PyFoam.Basics.TemplateFile import TemplateFile
from PyFoam.RunDictionary.SolutionDirectory import SolutionDirectory
from os import path
import numpy as np

case = SolutionDirectory(".", archive = None, paraviewLink = False )

nu = 1e-4
uTau = 0.0536289524525

deltaNu = nu/uTau

delta = 1
delta99 = 0.8

# Length in the  streamwise and spanwise directions
lX = 100*delta99
lZ = 6    

deltaXPlus = 50 
deltaYPlus = 2
deltaZPlus = 20 

# Height of the vscous sublayer
hVS = 0.0328266

# Number of points in the viscous regions
nVS = 9

# Number of points in the wake region
nW = 22

# Height of the end of the overlap layer
hOL = 0.255968

# Expansion ratio in the overlap layer
rOL = 7.79759

nOL = 18

# Number of points in first block above delta
nY1 = int(nW*delta/(delta-hOL))

# Number of points in second block above delta
nY2 = 60

rY2 = 6.555349157


# Calculating nX and nZ
deltaX = deltaXPlus*nu/uTau
deltaZ = deltaZPlus*nu/uTau

nX = int(lX/deltaX)
nZ = int(lZ/deltaZ)

nY = nVS + nOL + nW + nY1 + nY2


print "Total mesh size: ", nX, "x", nY, "x", nZ, "=", nX*nZ*nY


paramString = "{'lX':"+str(lX)+",'lZ':"+str(lZ)+",'nX':"+str(nX)+ \
    ",'nZ':"+str(nZ)+ \
    ",'nVS':"+str(nVS)+ \
    ",'nOL':"+str(nOL)+ \
    ",'nW':"+str(nW)+ \
    ",'rOL':"+str(rOL)+ \
    ",'hVS':"+str(hVS)+ \
    ",'hOL':"+str(hOL)+ \
    ",'delta':"+str(delta)+ \
    ",'nY1':"+str(nY1)+ \
    ",'nY2':"+str(nY2)+ \
    ",'rY':"+str(rY2)+ \
    "}"

print "Generating blockMeshDict\n"
call(["pyFoamFromTemplate.py", "constant/polyMesh/blockMeshDict", paramString])

