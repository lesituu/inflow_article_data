from __future__ import print_function
from __future__ import division
import os
import sys
import numpy as np
import matplotlib.pyplot as plt
import h5py as h5py
from mpi4py import MPI
from eddylicious.readers.foamfile_readers import read_points_from_foamfile
from eddylicious.readers.foamfile_readers import read_velocity_from_foamfile
from eddylicious.generators.helper_functions import chunks_and_offsets
import argparse
from scipy.interpolate import griddata

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
nProcs = comm.Get_size()


# Define the command-line arguments

precursorCaseDir = "."
surfaceName = "inletSurfaceNew"
uMeanFile = "uMean.txt"
fileName = "database.hdf5"

dataDir = os.path.join(precursorCaseDir, "postProcessing",
                       "sampledSurface")

# Grab the existing times and sort
times = os.listdir(dataDir)
times = np.sort(times)


#maxTime = 3000 

#idxT = np.argmin(np.abs(np.array(map(float, times))-maxTime))
#times = times[:idxT]

# Get the mean profile and append zeros
uMean = np.genfromtxt(uMeanFile)
uMeanX = uMean[:, 1]
if uMean.shape[1] == 3:
    uMeanY = uMean[:, 2]
else:
    uMeanY = np.zeros(uMeanX.shape)

y = uMean[:, 0]

# Read in the points
[pointsY, pointsZ, yInd, zInd] = read_points_from_foamfile(
    os.path.join(dataDir, times[0], surfaceName, "faceCentres"),
    addValBot=0, addValTop=8)

pointsY = np.column_stack((pointsY[:,0], pointsY, pointsY[:, 0]))
pointsZ = np.column_stack((np.zeros([pointsY.shape[0], 1]), pointsZ,
                           6*np.ones([pointsY.shape[0], 1])))

[pointsYReal, pointsZReal, yIndReal, zIndReal] = read_points_from_foamfile(
    os.path.join(precursorCaseDir, "postProcessing", "surfaces", "0", "inlet",
                 "faceCentres"),
    addValBot=0, addValTop=8)

[nPointsY, nPointsZ] = pointsY.shape
[nPointsYReal, nPointsZReal] = pointsYReal.shape

assert (nPointsY-2)/2. == uMeanX.size-2

# Allocate arrays for the fluctuations
if rank == 0:
    if os.path.isfile(fileName):
        print("HDF5 file already exists. It it will be overwritten.")
        os.remove(fileName)

dbFile = h5py.File(fileName, 'a', driver='mpio', comm=MPI.COMM_WORLD)

pointsGroup = dbFile.create_group("points")
velocityGroup = dbFile.create_group("velocity")

pointsGroup.create_dataset("pointsY", data=pointsYReal)
pointsGroup.create_dataset("pointsZ", data=pointsZReal)

velocityGroup.create_dataset("uMeanX", data=uMeanX)
velocityGroup.create_dataset("uMeanY", data=uMeanY)

velocityGroup.create_dataset("times", data=[float(times[i])
                                            for i in range(times.size)])

uX = velocityGroup.create_dataset("uX", (len(times),
                                            pointsYReal.shape[0],
                                            pointsYReal.shape[1]),
                                    dtype=np.float64)
uY = velocityGroup.create_dataset("uY", (len(times),
                                            pointsYReal.shape[0],
                                            pointsYReal.shape[1]),
                                    dtype=np.float64)
uZ = velocityGroup.create_dataset("uZ", (len(times),
                                            pointsYReal.shape[0],
                                            pointsYReal.shape[1]),
                                    dtype=np.float64)

dbFile.attrs["nPointsY"] = pointsYReal.shape[0]
dbFile.attrs["nPointsZ"] = pointsYReal.shape[1]
dbFile.attrs["nPoints"] = pointsYReal.size

[chunks, offsets] = chunks_and_offsets(nProcs, len(times))

readFunc = read_velocity_from_foamfile(dataDir, surfaceName,
                                       nPointsZ-2, yInd, zInd,
                                       addValBot=(0, 0, 0),
                                       addValTop=(uMeanX[-1], uMeanY[-1], 0))

pointsY = pointsY.flatten()
pointsZ = pointsZ.flatten()

# Read in the fluctuations
for i in range(chunks[rank]):
    if rank == 0 and (np.mod(i, int(chunks[rank]/20)) == 0):
        print("Converted about " + str(i/chunks[rank]*100)+"%")

    position = offsets[rank] + i
    # Read in U
    [uXVal, uYVal, uZVal] = readFunc(times[position])
    
    uXVal = np.column_stack((uXVal[:,0], uXVal, uXVal[:, -1])).flatten()
    uYVal = np.column_stack((uYVal[:,0], uYVal, uYVal[:, -1])).flatten()
    uZVal = np.column_stack((uZVal[:,0], uZVal, uZVal[:, -1])).flatten()
    uXInterp = griddata((pointsZ, pointsY), uXVal, (pointsZReal, pointsYReal),
                        method="linear")
    uYInterp = griddata((pointsZ, pointsY), uYVal, (pointsZReal, pointsYReal),
                        method="linear")
    uZInterp = griddata((pointsZ, pointsY), uZVal, (pointsZReal, pointsYReal),
                        method="linear")

    uX[position, :, :] = uXInterp
    uY[position, :, :] = uYInterp
    uZ[position, :, :] = uZInterp

if rank == 0:
    print("Process 0 done, waiting for the others...")

comm.Barrier()
if rank == 0:
    print("Done")
