from subprocess import call
from PyFoam.Basics.TemplateFile import TemplateFile
from PyFoam.RunDictionary.SolutionDirectory import SolutionDirectory
from os import path
import numpy as np

case = SolutionDirectory(".", archive = None, paraviewLink = False )

nu = 1e-4
uTau = 0.055

print "Re_tau = ", uTau/nu

# Length in the  streamwise and spanwise directions
lX = 9
lZ = 6    

# Height of the vscous sublayer
hVS = 0.0328266

# Height of the end of the overlap layer
hOL = 0.255968

# Mesh size and grading

# Streamwise and spanwise direction
deltaXPlus = 50 
deltaZPlus = 20 

# Wall-normal direction

# Number of points in the viscous regions
nVS = 9 

# Number of points in the overlap layer
nOL = 18 

# Expansion ration in the overlap layer
rOL = 7.79759

# Number of poins in the wake region
nW = 22


# Calculating nX and nZ
deltaX = deltaXPlus*nu/uTau
deltaZ = deltaZPlus*nu/uTau

nX = int(lX/deltaX)
nZ = int(lZ/deltaZ)

nY = nVS + nOL + nW


print "Total mesh size: ", nX, "x", 2*nY, "x", nZ, "=", 2*nX*nZ*nY


paramString = "{'lX':"+str(lX)+",'lZ':"+str(lZ)+",'nX':"+str(nX)+ \
    ",'nZ':"+str(nZ)+ \
    ",'nVS':"+str(nVS)+ \
    ",'nOL':"+str(nOL)+ \
    ",'nW':"+str(nW)+ \
    ",'rOL':"+str(rOL)+ \
    ",'hVS':"+str(hVS)+ \
    ",'hOL':"+str(hOL)+ \
    "}"

print "Generating blockMeshDict\n"
call(["pyFoamFromTemplate.py", "constant/polyMesh/blockMeshDict", paramString])

