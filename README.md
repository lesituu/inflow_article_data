# README #

This repository contains data from channel flow and and turbulent boundary layer simulations presented in

* T. Mukha and M. Liefvendahl. The generation of turbulent inflow boundary conditions using precursor channel flow simulations, [diva2:967721](http://www.diva-portal.org/smash/record.jsf?pid=diva2%3A967721).
* T. Mukha. Inflow generation for scale-resolving simulations of turbulent boundary layers. Licentiate thesis 2016-009, Uppsala Univeristy, [diva2:967753](http://www.diva-portal.org/smash/record.jsf?pid=diva2%3A967753).

Kindly, cite the above publications if you make use of the data.

For comments or inqueries please contact

timofey dot mukha at it.uu.se

mattias dot liefvendahl at foi.se

## Contents ##

The repository contains the following data.

* Four OpenFOAM cases including mesh specification, set up and initial conditions. They correspond to the four cases presented in the publications:
    * The precursor channel flow simulation.
    * The TBL simulation using the proposed inflow generation method.
    * The TBL simulation using a uniform inflow velocity.
    * The TBL simulation using the rescaling method.
* Averaged flow field data for each of the cases.

## Getting the data ##

Data for each TBL case are presented in two formats, as a .vtm file and as dsv.
In both cases the data represents the computed flow fields averaged in time and across the spanwise direction, the data are therefore 2D.
In case of discrepencies between the two datasets (which should be small), dsv data takes precedence.
To save space the data is compressed with gzip.

For the channel flow case the data is given in dsv format only, in the form of 1d profiles.

### The dsv data ###
The dsv data is located in the directory averaged_data_dsv in each of the cases. 
The x and y files contain 1D arrays of coordinates, the whole mesh is formed as their Cartesian product.
The tbl_properties file contains characteristics of the TBL as a function of x: u_tau, y+, delta_99, delta_*, theta and H.
The other files correspond to 2D fields of the flow variables, namely first and second order statistical moments of the velocity field.
The quantities are given without any scaling, but scaling can easily be applied by using the TBL properties data.
The rows in the data correspond to y, and the columns to x.

### The vtm data ###
The vtm format is a VTK format for multiblock datasets.
The location of the data is in the averaged_data_vtm folder in each case.
The .vtm file inside can be opened with, for instance, Paraview, to visually analyse the data.
Each dataset consists of several blocks, one block representing the data at the centres of the finite-volume cells, and each other block represents data on the boundary.
So the .vtm file is basically a container, the members of which are individual blocks, stored as .vtp (VTK polydata) files in the vtm_data directory located next to the .vtm file.
It is therefore possible to directly inspect each .vtp file individually if one so desires.
Note that this data is considered unstructured, so for quick post-processing the dsv format is recommended, where it is easy to take advantage of the rectangular mesh structure.

## Reproducing the results ##

Some general comments

* The cases are meant to run with OpenFOAM version 3.0.1.
* pimpleFoam is to be used as the solver.
* The meshes are to be generated with blockMesh, which is part of OpenFOAM. If one would like to change the mesh without changing its topology, a convenient way is to alter the paramters in the .py file present in the "constant" directory for each case. This file can be used in conjunction with the .template file also present in the same directory to generate a blockMeshDict corresponding to the new parameters. This is done with the help of PyFoam --- a python package that makes many operations with OpenFOAM cases convenient.
See these [slides](http://web.student.chalmers.se/groups/ofw5/Advanced_Training/pyFoamAdvanced.pdf) to learn more about PyFoam and templates. Naturally, if you use a different mesh the results will differ from data provided here.
* In all cases you should adjust the endTime in the control dict to the desired "transient removal" period, and switch the field averaging and sampling function objects off.
Then the endTime should be readjusted according to the desired averaging time and function objects enabled.
We recommend to run for at least 2000 simulations seconds to get converged results.

### The precursor channel flow

* The initial conditions provided in the repository give a uniform pressure and velocity field. This may not be a suitable start to get turbulence developed. We recommend using [perturbU](https://github.com/wyldckat/perturbU) to perturb the velocity field before running the simulation.
* To average results in the x and z direction and get 1d profiles we recommend using the [postChannelFlow](https://bitbucket.org/lesituu/postchannelflow) utility.

### The TBL simulations

* To get z-averaged dsv results we recommend using [averageAlongAxis](https://bitbucket.org/lesituu/averagealongaxis).
* To get z-averaged results in the vtm format one can use the averageAlongAxis script in the python package [turbulucid](https://github.com/timofeymukha/turbulucid).

The most tedious task is generating the turbulent inflow for the two simulations that require it.
In principle one needs to read in the velocity distributions sampled by the precursor or the TBL simulation with uniform inflow, manipulate it, and save them into a format that OpenFOAM uses for boundary data.
In the case employing the rescaling one also need to apply the rescaling procedure.
On the solver's side one needs a special boundary condition that allows to update the boundary values at each time-step -- timeVaryingMappedFixedValue. This is already configured in the initial condition file for the velocity.

In order to make the above less of a hassle we recommend using [eddylicious](https://github.com/timofeymukha/eddylicious) --- a python package dedicated to inflow generation.
The package is in fact a by product of our work with inflow generation for TBLs.
It is well [documented](http://eddylicious.readthedocs.io), in particular using it with OpenFOAM.

Currently, eddylicious will not be able to handle the data sampled in the TBL case with uniform inflow.
This is due to the fact that OpenFOAM will insist on triangulating the sampling surface, and eddylicious only supports data arranged in a rectangular grid.
To fix this, the script convert.py is provided in the uniform_inflow case.
It will read the sampled data and re-sample it back to a rectangular grid.
The results will be saved into a single hdf5 file which can be used by eddylicious.
The convert.py script will need to know what the mean velocity is at the sampling location.
We provide a file, uMean.txt with 3 columns corresponding to y, U and V. Naturally, you can use your own data to compute the mean and save it in a similar format.